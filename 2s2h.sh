#!/bin/sh
# 2Ship2Harkinian Setup Script

configdir="/home/$USER/.config/2s2h"
Sysdir="/usr/share/games/2s2h"
export LD_LIBRARY_PATH=/usr/lib/2s2h:$LD_LIBRARY_PATH

 if [ -d "$configdir" ]; then
  ### Take action if $configdir exists ###
  echo "Game folder exist, running game"
else
mkdir $configdir
# import existing oot file
zenity --question --text="Do you want to import your existing mm.o2r file? skip if this is your first time setting up 2Ship2Harkinian"

# Get the exit status of Zenity (Yes is 0, No is 1)
response=$?

# Check if the user pressed 'Yes' (exit status 0)
if [ $response -eq 0 ]; then
    # Perform an action if Yes is pressed
    # Show a Zenity file selection dialog and store the selected file's path
file=$(zenity --file-selection --title="Select the mm.o2r file" --file-filter="*.o2r")

# Check if the user selected a file (i.e., the file path is not empty)
if [ -n "$file" ]; then
    
    # Ensure the target directory exists
    if [ ! -d "$configdir" ]; then
        mkdir -p "$configdir"
    fi
    
    # Move the selected file to the target directory
    mv "$file" "$configdir"

    # Display a success message
    zenity --info --text="mm.o2r imported successfully to $configdir"
else
    # If no file was selected, show a cancellation message
    zenity --warning --text="No file selected, operation cancelled."
fi
else
    # Skip the action if No is pressed
    echo "Skipping import"
fi
ln -s $Sysdir/2ship.o2r $configdir
ln -s $Sysdir/assets $configdir
ln -s $Sysdir/2s2h $configdir
fi
  echo "Running game"
  cd $configdir 
  ./2s2h
